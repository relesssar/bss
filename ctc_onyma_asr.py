#!/usr/bin/python
# -*- coding: utf-8 -*-
# Применяет на железе поступающие события с веб-интерфейса ONYMA,
# входящие данные:
# event= PAUSE - Пауза, или ACTIVATE Вкл-(убераем паузу), или UPDATE Изменение скорости
# client_ip= Айпи клиента
# p_clsrv= айди соеденения в ониме, для отчётов по почте.
#  тестовый айпи 188.0.137.60 , Лицевой счет N 4445 - test_ctc

import os
import logging
import requests
import datetime
import time
# import ctc_onyma_conf as conf
# import getopt
import sys
import paramiko
import re
import vip_ip as vip

# import xmltodict
# import json
#'--p_router=GW0.ATR1-217.196.16.14', '--p_speed=512']
def runAsr(client_ip='', event='', p_clsrv='', p_user='', p_user_email='', p_router='', p_speed=''):

    import ctc_onyma_send_mail as mail
    logging.info(u'ASR client_ip={}'.format(client_ip))
    ip_l = client_ip.split(',')

    for i in ip_l:
        # может прилетать в двух форматах
        # 'test_ctc: 188.0.137.60/32,188.0.137.62/32,188.0.137.67/32,188.0.137.68/32'
        # '188.0.137.68/32;188.0.137.62/32;188.0.137.60/32;188.0.137.67/32',
        tmp1 = i.split(": ")
        logging.debug(tmp1)
        try:
            tmp = tmp1[1].split("/")
        except IndexError:
            tmp = i.split("/")

        if event not in {'pause', 'activate'}:
            logging.info(u' exit script, event not in pause, activate client_ip {} {}'.format(event, tmp))
            sys.exit(2)
        # vip client
        if tmp[0] in vip.vip_ip_l:
            logging.info(u'{} vip клиент!'.format(tmp[0]))
            mail.sendEmail('error, vip клиент \n --event={} --client_ip={}  --p_user={}'.format(event, tmp[0], p_user),
                           'ONYMA EVENT ERROR, Vip client', p_user_email)
            sys.exit(2)

        logging.info(u'client_ip {} {}'.format(event, tmp))
        setEvent(event=event, client_ip=tmp[0], p_clsrv=p_clsrv, p_user=p_user, p_user_email=p_user_email, p_router=p_router
                 , p_speed=p_speed)
        logging.info(u'{} {} - OK'.format(event, tmp[0]))
        msg_email = '\nOK\nUSER_ID: {}\nID: {}\nClient IP: {}\n Event: {} \n{}\n'.format(p_user, p_clsrv, client_ip, event,tmp[0])
        mail.sendEmail(msg_email, 'ONYMA EVENT OK', p_user_email)

    sys.exit(2)


# Делаю проверку на существование интерфейса, если не существует то пропускаю команду и высылаю письмо.
# return True Если существует.
def checkInterface(remote_conn, i, event, client_ip, p_clsrv, p_user, p_user_email):
    import ctc_onyma_send_mail as mail
    if i['router_os'] == 'ios':
        remote_conn.send('sho run inter {}\n'.format(i['iface_name']))
        logging.debug('sho run inter {}'.format(i['iface_name']))
        while not remote_conn.recv_ready():
            time.sleep(0.5)
        out = remote_conn.recv(65535)
        logging.debug('out->{}'.format(out))
        if re.findall(r'Invalid input detected', out):
            msg_email = '\nUSER_ID: {}\nID: {}\nClient IP: {}\n Event: {} \n Interface: {}\n {}'.format(p_user, p_clsrv,
                                                                                                   client_ip, event,
                                                                                                   i['iface_name'], out)
            # письмо с ошибкой
            mail.sendEmail(msg_email, 'ONYMA Invalid input detected ', p_user_email)
            logging.debug(msg_email)
            return False

    elif i['router_os'] == 'xr':
        remote_conn.send('sho run inter {}\n'.format(i['iface_name']))
        logging.debug('sho run inter {}'.format(i['iface_name']))
        while not remote_conn.recv_ready():
            time.sleep(0.5)
        out = remote_conn.recv(65535)
        logging.debug('out->{}'.format(out))
        if re.findall(r'No such configuration', out):
            msg_email = '\nUSER_ID: {}\nID: {}\nClient IP: {}\n Event: {} \n Interface: {}\n {}'.format(p_user, p_clsrv,
                                                                                                   client_ip, event,
                                                                                                   i['iface_name'], out)
            # письмо с ошибкой
            mail.sendEmail(msg_email, 'ONYMA No such configuration ', p_user_email)
            logging.debug(msg_email)
            return False
    logging.debug("checkInterface({})-> True".format(i['iface_name']))
    return True


# выполнение заданных событий
def setEvent(event, client_ip, p_clsrv, p_user, p_user_email, p_router='', p_speed=''):
    import ctc_onyma_send_mail as mail
    import ctc_onyma_mysql_query as mysql

    logging.info('start setEvent')
    p_router_l = p_router.split('-')
    p_router_host, p_router_name = "", ""
    if len(p_router_l) != 0:
        p_router_name = p_router_l[0].strip()
        p_router_host = p_router_l[1].strip()

    # ищем роутер(маршрутизатор)
    # результат  {'id': , 'router_user': , 'router_pass':
    # , 'router_name': , 'router_ip': , 'router_port': , 'router_os':
    # , 'router_descr': ,'iface_name':}
    mysql_obj = mysql.MysqlDb()
    router_l = mysql_obj.getRouterByClientIp(event=event, client_ip=client_ip, p_clsrv=p_clsrv, p_user=p_user,
                                             p_user_email=p_user_email)

    #logging.info('router_l= ')
    logging.info('mysql=============================================')
    logging.info(router_l)
    logging.info('mysql=============================================')
    #row = router_l.fetchone()
    #logging.info(vars(router_l[0]))

    #for row in router_l.fetchall():
    #    logging.info('row[0]')
    #    logging.info(row[0])

    remote_conn_pre = paramiko.SSHClient()
    remote_conn_pre.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    #print vars(router_l)

    for i in router_l:
        print('router_ip={}'.format(i['router_ip']))
        msg_email = '\nUSER_ID: {}\nID: {}\nClient IP: {}\n Event: {}\n Router-Onyma: {} \n Router: {}  ERROR'.format(p_user, p_clsrv, client_ip, event, p_router, i['router_ip'])

        # Если с Онимы роутер айпи не совпадает с найденым то сообщение инженерам
        if p_router_host != i['router_ip']:
            # письмо с ошибкой
            e = 'Warning! \n Router-Onyma info \n{} != {} \n{}'.format(p_router, i['router_ip'], msg_email)
            logging.debug(e)
            mail.sendEmail(e, 'ONYMA ERROR Router info', p_user_email)
            sys.exit(2)
            
        # задаем условие цикла
        attempts = 0
        while attempts < 2:
            attempts += 1
            print('attempts', attempts)
            # Выполняем попытку в пределах цикла
            cmd_out = ""
            try:
                remote_conn_pre.connect(hostname=i['router_ip'], username=i['router_user'], password=i['router_pass'],
                                        port=int(i['router_port']),
                                        look_for_keys=False, allow_agent=False)
            except (paramiko.ssh_exception.BadHostKeyException, paramiko.ssh_exception.AuthenticationException,
                    paramiko.ssh_exception.SSHException, paramiko.ssh_exception.socket.error) as e:
                # письмо с ошибкой
                logging.info('=============================================')
                logging.info('ONYMA EVENT ACL ERROR connect server')
                logging.info(i['router_ip'])
                logging.info(i['router_user'])
                logging.info(i['router_pass'])
                logging.info(i['router_port'])
                logging.info('=============================================')
                mail.sendEmail(str(e), 'ONYMA EVENT ACL ERROR connect server', p_user_email)
            else:
                remote_conn = remote_conn_pre.invoke_shell()
                remote_conn.send("terminal length 0\n")
                logging.debug('terminal length 0')
                while not remote_conn.recv_ready():
                    time.sleep(0.5)
                cmd_out += remote_conn.recv(65535)

                # Делаю проверку на существование интерфейса, если не существует то пропускаю команду и высылаю письмо.
                if checkInterface(remote_conn, i, event, client_ip, p_clsrv, p_user, p_user_email) is False:
                    continue

                # войти в режим конфигурации
                remote_conn.send('conf t' + '\n')
                logging.debug('conf t')
                while not remote_conn.recv_ready():
                    time.sleep(0.2)
                cmd_out += remote_conn.recv(65535)
                logging.info('=============================================')
                logging.info('router_os')
                logging.info(i['router_os'])
                logging.info('=============================================')
                if i['router_os'] == 'ios':
                    remote_conn.send('inter {}\n'.format(i['iface_name']))
                    logging.debug('inter {}'.format(i['iface_name']))
                    while not remote_conn.recv_ready():
                        time.sleep(0.2)
                    cmd_out += remote_conn.recv(65535)

                    if event == 'pause':  # PAUSE PAUSE PAUSE PAUSE PAUSE   PAUSE PAUSE PAUSE PAUSE PAUSE
                        # remote_conn.send('ip access-group  DENY_ALL in\n')
                        # logging.debug('ip access-group  DENY_ALL in')
                        remote_conn.send('shutdown\n')
                        logging.debug('shutdown')
                        while not remote_conn.recv_ready():
                            time.sleep(0.2)
                        cmd_out += remote_conn.recv(65535)
                    elif event == 'activate':  # ACTIVATE ACTIVATE ACTIVATE ACTIVATE ACTIVATE ACTIVATE ACTIVATE ACTIVATE
                        # remote_conn.send('no ip access-group  DENY_ALL in\n')
                        # logging.debug('no ip access-group  DENY_ALL in')
                        remote_conn.send('no shutdown\n')
                        logging.debug('no shutdown')
                        while not remote_conn.recv_ready():
                            time.sleep(0.5)
                        cmd_out += remote_conn.recv(65535)
                    remote_conn.send('exit\n')
                    # print('no ip access-group  DENY_ALL in')
                    while not remote_conn.recv_ready():
                        time.sleep(0.5)
                    cmd_out += remote_conn.recv(65535)
                    # print(out)
                    remote_conn.send('exit\n')
                    # print('no ip access-group  DENY_ALL in')
                    while not remote_conn.recv_ready():
                        time.sleep(0.5)
                    cmd_out += remote_conn.recv(65535)
                    # print(out)
                    # проверяю изменения , приминились или нет
                    remote_conn.send('sho run inter {}\n'.format(i['iface_name']))
                    logging.debug('sho run inter {}'.format(i['iface_name']))
                    while not remote_conn.recv_ready():
                        time.sleep(0.5)
                    out = remote_conn.recv(65535)
                    cmd_out += out
                    # ip access-group DENY_ALL in
                    if event == 'pause':
                        if re.findall(r'shutdown', out):
                            msg_email = '\nUSER_ID: {}\nID: {}\nClient IP: {}\n Event: {} OK'.format(p_user, p_clsrv,
                                                                                                     client_ip, event)
                            mysql_obj.setClientStatus(i['client_id'], event)
                            #  mysql_obj.setClientHistory(p_user, event, p_clsrv, cmd_out, i['client_id'], client_ip,script_status='ok')
                            break
                        else:
                            msg_email = '\nпопытка № {}\nUSER_ID: {}\nID: {}\nClient IP: {}\n Event: {} ERROR'.format(
                                attempts, p_user, p_clsrv,
                                client_ip, event)
                            mail.sendEmail(msg_email, 'ONYMA EVENT ACL ON/OF ERROR попытка № {}'.format(attempts),
                                           p_user_email)
                    # no ip access-group DENY_ALL in
                    if event == 'activate':
                        if len(re.findall(r'shutdown', out)) == 0:

                            msg_email = '\nUSER_ID: {}\nID: {}\nClient IP: {}\n Event: {} OK'.format(p_user, p_clsrv,
                                                                                                     client_ip, event)
                            mysql_obj.setClientStatus(i['client_id'], event)
                            # mysql_obj.setClientHistory(p_user, event, p_clsrv, cmd_out, i['client_id'], client_ip, script_status='ok')
                            break
                        else:
                            msg_email = '\nпопытка № {}\nUSER_ID: {}\nID: {}\nClient IP: {}\n Event: {} error'.format(
                                attempts, p_user, p_clsrv,
                                client_ip, event)
                            mail.sendEmail(msg_email, 'ONYMA EVENT ACL ON/OF ERROR попытка № {}'.format(attempts),
                                           p_user_email)
                elif i['router_os'] == 'xr':
                    remote_conn.send('interface {}\n'.format(i['iface_name']))
                    logging.debug('interface {}'.format(i['iface_name']))
                    while not remote_conn.recv_ready():
                        time.sleep(0.2)
                    cmd_out += remote_conn.recv(65535)

                    if event == 'pause':
                        # remote_conn.send('ipv4 access-group DENY_ALL ingress\n')
                        # logging.debug('ipv4 access-group DENY_ALL ingress')
                        remote_conn.send('shutdown\n')
                        logging.debug('shutdown')
                        while not remote_conn.recv_ready():
                            time.sleep(0.2)
                        cmd_out += remote_conn.recv(65535)

                    elif event == 'activate':  # ACTIVATE ACTIVATE ACTIVATE ACTIVATE ACTIVATE ACTIVATE ACTIVATE ACTIVATE
                        # remote_conn.send('no ipv4 access-group DENY_ALL ingress\n')
                        # logging.debug('no ipv4 access-group DENY_ALL ingress')
                        remote_conn.send('no shutdown\n')
                        logging.debug('no shutdown')
                        while not remote_conn.recv_ready():
                            time.sleep(0.5)
                        cmd_out += remote_conn.recv(65535)
                    remote_conn.send('commit\n')
                    # print('no ip access-group  DENY_ALL in')
                    while not remote_conn.recv_ready():
                        time.sleep(0.5)
                    out = remote_conn.recv(65535)
                    print(out)
                    cmd_out += out
                    # remote_conn.send('Y')
                    # print('no ip access-group  DENY_ALL in')
                    # while not remote_conn.recv_ready():
                    # time.sleep(0.5)
                    # out = remote_conn.recv(65535)
                    # print(out)
                    # cmd_out += out
                    remote_conn.send('exit\n')
                    # print('no ip access-group  DENY_ALL in')
                    while not remote_conn.recv_ready():
                        time.sleep(0.5)
                    cmd_out += remote_conn.recv(65535)
                    remote_conn.send('exit\n')
                    # print('no ip access-group  DENY_ALL in')
                    while not remote_conn.recv_ready():
                        time.sleep(0.5)
                    cmd_out += remote_conn.recv(65535)
                    # проверяю изменения , приминились или нет
                    remote_conn.send('sho run inter {}\n'.format(i['iface_name']))
                    logging.debug('sho run inter {}'.format(i['iface_name']))
                    while not remote_conn.recv_ready():
                        time.sleep(0.5)
                    out = remote_conn.recv(65535)
                    print(out)
                    cmd_out += out
                    # ip access-group DENY_ALL in
                    if event == 'pause':
                        if re.findall(r'shutdown', out):
                            msg_email = '\nUSER_ID: {}\nID: {}\nClient IP: {}\n Event: {} OK'.format(p_user, p_clsrv,
                                                                                                     client_ip, event)
                            mysql_obj.setClientStatus(i['client_id'], event)
                            #mysql_obj.setClientHistory(p_user, event, p_clsrv, cmd_out, i['client_id'], client_ip, script_status='ok')
                            break
                        else:
                            msg_email = '\nпопытка № {}\nUSER_ID: {}\nID: {}\nClient IP: {}\n Event: {} error'.format(
                                attempts, p_user, p_clsrv, client_ip, event)
                            mail.sendEmail(msg_email, 'ONYMA EVENT ACL ON/OF ERROR попытка № {}'.format(attempts),
                                           p_user_email)
                    # no ip access-group DENY_ALL in
                    if event == 'activate':
                        if len(re.findall(r'shutdown', out)) == 0:
                            msg_email = '\nUSER_ID: {}\nID: {}\nClient IP: {}\n Event: {} OK'.format(p_user, p_clsrv,
                                                                                                     client_ip, event)
                            mysql_obj.setClientStatus(i['client_id'], event)
                            # mysql_obj.setClientHistory(p_user, event, p_clsrv, cmd_out, i['client_id'], client_ip, script_status='ok')
                            break
                        else:
                            msg_email = '\nпопытка № {}\nUSER_ID: {}\nID: {}\nClient IP: {}\n Event: {} error'.format(
                                attempts, p_user, p_clsrv, client_ip, event)
                            mail.sendEmail(msg_email, 'ONYMA EVENT ACL ON/OF ERROR попытка № {}'.format(attempts),
                                           p_user_email)
                remote_conn.close()
                remote_conn_pre.close()
        print(msg_email)
        logging.debug(msg_email)
        mail.sendEmail(msg_email, 'ONYMA EVENT ACL', p_user_email)


# Функция disable_paging обеспечивает необходимые параметры SSHClient для подключение к Cisco IOS XR
def disable_paging(remote_conn):
    remote_conn.send("terminal length 0\n")
    time.sleep(2)
    output = remote_conn.recv(1000)
    return output


'''
//XR 
//PAUSE
RP/0/RSP0/CPU0:bb10.alm1#conf t Fri Feb 9 15:23:06.725 AST 
RP/0/RSP0/CPU0:bb10.alm1(config)#interface Bundle-Ether30.451 
RP/0/RSP0/CPU0:bb10.alm1(config-subif)#shutdown 
RP/0/RSP0/CPU0:bb10.alm1(config-subif)#commit Fri Feb 9 15:23:20.195 AST 
RP/0/RSP0/CPU0:bb10.alm1(config-subif)#exit 
RP/0/RSP0/CPU0:bb10.alm1(config)#exit 

//ACTIVATE
RP/0/RSP0/CPU0:bb10.alm1#
RP/0/RSP0/CPU0:bb10.alm1#conf t Fri Feb 9 15:23:48.158 AST Current Configuration Session Line User Date Lock 00000001-00030067-0000018b SYSTEM SYSTEM Fri Feb 9 15:23:30 2018 
RP/0/RSP0/CPU0:bb10.alm1(config)#interface Bundle-Ether30.451 
RP/0/RSP0/CPU0:bb10.alm1(config-subif)#no shutdown 
RP/0/RSP0/CPU0:bb10.alm1(config-subif)#commit 
RP/0/RSP0/CPU0:bb10.alm1(config-subif)pip install python-ESL
#exit 
RP/0/RSP0/CPU0:bb10.alm1(config)#exit 
RP/0/RSP0/CPU0:bb10.alm1#

'''

'''
// IOS


sho running-config  //  посмотреть всю текущую конфигурацию

conf t // войти в режим конфигурации


//что бы  изменить настройки интерфейса клиента необходимо
//1 войти в режим конфигурации
//2 войти в нужный интерфейс
_____________________________________________________
// пример блокировки
conf t
inter GigabitEthernet2.20
ip access-group  DENY_ALL in
// после внесений изменений в конфигурацию считываем конфигурацию интерфейса убеждаемся что конфигурация применилась
// sho run inter GigabitEthernet2.20

//пример разблокировки
conf t
inter GigabitEthernet 2.20
no ip access-group  DENY_ALL in
// после внесений изменений в конфигурацию считываем конфигурацию интерфейса убеждаемся что конфигурация применилась
// sho run inter GigabitEthernet2.20

____________________________________________________

// пример изменения скорости
// небходимо считать текущую настройку интерфейса узнать какой полиси применен
// sho run inter GigabitEthernet 2.20
sho run inter GigabitEthernet 2.20
Building configuration...

Current configuration : 162 bytes
!
interface GigabitEthernet2.10
encapsulation dot1Q 10
ip address 20.20.20.253 255.255.255.0
service-policy input 1024_Meter
изменяем на 5 Мб/c

conf t
interface GigabitEthernet2.10
no service-policy input 1024_Meter
service-policy input 5120_Meter
// после внесений изменений в конфигурацию считываем конфигурацию интерфейса убеждаемся что конфигурация применилась
// sho run inter GigabitEthernet2.10

'''
