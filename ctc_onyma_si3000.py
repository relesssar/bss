#!/usr/bin/python
# -*- coding: utf-8 -*-
# Применяет на железе поступающие события с веб-интерфейса ONYMA,
# входящие данные:
# event= PAUSE - Пауза, или ACTIVATE Вкл-(убераем паузу)
#  тестовый айпи 188.0.137.60 , Лицевой счет N 4445 - test_ctc
#import ctc_onyma_conf as conf

import logging
import requests
import datetime
import sys
import xmltodict

DEBUG = False


# исключение из white list номера на транкгруппах <> 0
def remove_from_white_list(phone_to_delete, origin_list):
    modyfied_list = []
    modyfied_list_length = []
    #print('origin_list = ', origin_list)

    def findposition(number, array):
        pos = 0
        for item in array:
            if number == item:
                return pos
            pos = pos + 1
        return None

    def findlessmore(number, array):
        j = 0
        for item in array:
            if number == item:
                return None
            if number < item:
                return j - 1
            j = j + 1
        for item in array:
            if number == item:
                return None
            if number > item:
                return j - 1
            j = j + 1

    def findlessmore_modified(number, array):
        j = 0
        for item in array:
            if number == item:
                return j
            if number < item:
                return j - 1
            j = j + 1
        for item in array:
            if number == item:
                return j
            if number > item:
                # print ( 'numbers = ', modyfied_list_length[findposition(modyfied_list_sort[number],modyfied_list)] )
                return j - 1
            j = j + 1

    #print('phone_to_delete  = ', phone_to_delete)

    # если иключаемый номер есть в списке и совпадает
    if findlessmore(phone_to_delete, origin_list) == None:

        # remove operation
        result = {}
        result["status"] = "remove"
        return result

        # python2
        #new_origin_list = origin_list.copy()
        new_origin_list = origin_list[:]

        new_origin_list.remove(phone_to_delete)
        #print('!new_origin_list = ', new_origin_list)
        return (new_origin_list)
        # привести список к 10 значным значениям
    i = 0
    for val in origin_list:
        # print(len(str(val)))
        if (len(str(val)) < 10):
            temp_val = val * pow(10, 10 - len(str(val)))
            modyfied_list.append(temp_val)
            modyfied_list_length.append(len(str(val)))
        else:
            modyfied_list.append(val)
            modyfied_list_length.append(len(str(val)))
        i = +1
    if DEBUG: print('modyfied list  = ', modyfied_list)
    if DEBUG: print('modyfied list length  = ', modyfied_list_length)

    # python2
    #modyfied_list_sort = modyfied_list.copy()
    modyfied_list_sort = modyfied_list[:]

    modyfied_list_sort.sort()
    if DEBUG: print('modyfied list sort  = ', modyfied_list_sort)

    j = 0
    findlessmore_val = findlessmore_modified(phone_to_delete, modyfied_list_sort)
    if DEBUG: print('findlessmore = ', findlessmore_val)
    if findlessmore_val != None:
        findposition_val = findposition(modyfied_list_sort[findlessmore_val], modyfied_list)
    else:
        findposition_val = None

    if DEBUG: print('findposition = ', findposition_val)

    # переводим диапазон в список
    range_to_list = []
    if findposition_val != None:
        if modyfied_list_length[findposition_val] != 10:
            # number in range !

            number_to_go = modyfied_list_sort[findlessmore_val]
            if DEBUG: print('number_to_go = ', number_to_go)
            pow0 = 10 - modyfied_list_length[findposition(modyfied_list_sort[findlessmore_val], modyfied_list)]
            if DEBUG: print('TEST= ', phone_to_delete - number_to_go)
            if DEBUG: print('pow', pow(10, pow0))
            if (phone_to_delete - number_to_go) > pow(10, pow0):
                #print('NOT in range!!! origin list with no change')
                test0 = None
            else:
                for i in range(pow(10, pow0)):
                    # for i in range(10):
                    if number_to_go != phone_to_delete:
                        # print (i,' => ',number_to_go)
                        range_to_list.append(number_to_go)
                    number_to_go = number_to_go + 1

    #print('range_to_list = ', range_to_list)

    # nothing operation
    if not range_to_list:
        result = {}
        result["status"] = "nothing"
        return result

    # Новый список white list
    if range_to_list:
        new_origin_list = []
        i = 0
        for lst in origin_list:
            if i == findposition_val:
                new_origin_list.extend(range_to_list)
            else:
                new_origin_list.append(lst)
            i = i + 1
    else:
        # python2
        #new_origin_list = origin_list.copy()
        new_origin_list = origin_list[:]
    #print('new_origin_list = ', new_origin_list)
    # modify operation
    result = {}
    result["status"] = "modify"
    result["data"] = range_to_list
    result["delete_position"] = findposition_val
    result["depth"] = 10 - pow0
    return result

def runSi3000(event='', p_atc='',client_ip='',p_clsrv='', p_user='',  p_atsid='', p_zone='', p_num='', p_tg='', p_username='', p_user_email=''): # Название функции не играет никакой роли, в принципе
#def run(): # Название функции не играет никакой роли, в принципе

    import ctc_onyma_send_mail as mail
    import ctc_onyma_mysql_query as mysql

    # test list #
    # test = remove_from_white_list(7272377244,[727237724, 7273442745])
    # print(test)
    # sys.exit(2)

    if p_tg != '' and p_tg == '4' and int(p_atsid)==26303:
        logging.info(u'! Тестовая Транк группа = {}'.format(p_tg))
        #print(u'! Тестовая Транк группа = {}'.format(p_tg))

        omn = OpenMN()
        omn.p_atsid = p_atsid
        omn.p_tg = p_tg

        # add record to White list
        # modif = omn.modifyWhiteBlacklist(['727343295'])
        # if ((modif == None) or modif['@code']=='3008'):
        #     print('Record added successfully!')
        # else:
        #     print('status = ', modif)
        # if modif == 'error':
        #     print('error')

        # remove record to White list
        # modif = omn.modifyWhiteBlacklist(['727343295'],True)
        # if ((modif == None) or modif['@code']=='3009'):
        #     print('Record removed successfully!')
        # else:
        #     print('status = ', modif)
        # if modif == 'error':
        #     print('error')

        #print(modif)
        #sys.exit(2)

        WL = omn.getWhiteBlacklist()
        #print(WL['node']['list']['@listType'])
        WL_list_origin = []
        WL_list_origin_legth = []
        WL_list_origin_mode = []
        if WL['node']['list']['@listType'] == 'white':
            if len(WL['node']['list']['paramList']) != 1:
                for list in WL['node']['list']['paramList']:
                    logging.info(u'list  {}'.format(list))
                    if list['param'][0]['@key'] == 'callingParty':
                        #print('callingParty=',list['param'][0]['@value'])
                        WL_list_origin.append(int(list['param'][0]['@value']))
                    if list['param'][1]['@key'] == 'length':
                        #print('length=',list['param'][1]['@value'])
                        WL_list_origin_legth.append(int(list['param'][1]['@value']))
                    if list['param'][2]['@key'] == 'lengthMode':
                        #print('lengthMode=',list['param'][2]['@value'])
                        WL_list_origin_mode.append(list['param'][2]['@value'])
            else:
                if WL['node']['list']['paramList']['param'][0]['@key'] == 'callingParty':
                    WL_list_origin.append(int(WL['node']['list']['paramList']['param'][0]['@value']))
                if WL['node']['list']['paramList']['param'][1]['@key'] == 'length':
                    WL_list_origin_legth.append(int(WL['node']['list']['paramList']['param'][1]['@value']))
                if WL['node']['list']['paramList']['param'][2]['@key'] == 'lengthMode':
                    WL_list_origin_mode.append(WL['node']['list']['paramList']['param'][2]['@value'])
        else:
            logging.info(u'no whitelist = {}'.format(WL['node']['list']))
            #print(u'no whitelist = {}'.format(WL['node']['list']))

        logging.info(u'! TEST ! ')

        logging.info(u'WL_list_origin  {}'.format(WL_list_origin))
        logging.info(u'WL_list_origin_legth  {}'.format(WL_list_origin_legth))
        logging.info(u'WL_list_origin_mode  {}'.format(WL_list_origin_mode))

        # check number to delete from White list
        omn.ar_code = p_zone.replace(' ', '').split('-')[1]
        omn.dn_num = p_num
        fullnumber = int(omn.ar_code + omn.dn_num)

        # turn off phone
        if event.lower() == 'pause':
            checkresult = remove_from_white_list(fullnumber, WL_list_origin)
            #print(checkresult)
            logging.info(u'checkresult {}'.format(checkresult))
            #sys.exit(2)
            if checkresult['status'] == 'nothing':
                #print('do nothing phone to delete not in range list')
                logging.info(u'do nothing phone to delete not in range list = {}'.format(fullnumber))
            if checkresult['status'] == 'remove':
                #print('remove exact number from white list = ',fullnumber)
                logging.info(u'remove exact number from white list = {}'.format(fullnumber))
                # remove
                query_result = omn.modifyWhiteBlacklist(fullnumber,True)
                #print('si3000 query result = ',query_result)
                logging.info(u'si3000 query result = {}'.format(query_result))
            if checkresult['status'] == 'modify':
                #print('modify white list, convert range to list . ',fullnumber)
                logging.info(u'modify white list, convert range to list . {}'.format(fullnumber))
                # remove range
                query_result = omn.modifyWhiteBlacklist(WL_list_origin[checkresult['delete_position']],True,checkresult['depth'])
                #print('si3000 query result = ',query_result)
                logging.info(u'si3000 query result = {}'.format(query_result))
                # add list
                query_result = omn.modifyWhiteBlacklist(checkresult['data'])
                #print('si3000 query result = ',query_result)
                logging.info(u'si3000 query result = {}'.format(query_result))
            sys.exit(2)
        if event.lower() == 'activate':
            # use the same method to remove, but waiting for status = nothing to add new number to white list
            checkresult = remove_from_white_list(fullnumber, WL_list_origin)
            #print(checkresult)
            logging.info(u'checkresult {}'.format(checkresult))
            if checkresult['status'] == 'nothing':
                query_result = omn.modifyWhiteBlacklist(fullnumber)
                if query_result == None:
                    #print('si3000 query result successful')
                    logging.info(u'si3000 query result successful')
                else:
                    #print('si3000 query result NOT successful,',query_result)
                    logging.info(u'si3000 query result NOT successful,{}'.format(query_result))
            if ((checkresult['status'] == 'modify') or (checkresult['status'] == 'remove')):
                #print('si3000 query result NOT successful, because number exist in white list', checkresult)
                logging.info(u'si3000 query result NOT successful, because number exist in white list, {}'.format(checkresult))
            sys.exit(2)

    logging.info(u' {}'.format(p_atc))
    if len(p_tg) != 0 and p_tg != '0':
        logging.info(u' ничего не делаю, так как Транк группа = {}'.format(p_tg))
        #print(u' ничего не делаю, так как Транк группа = {}'.format(p_tg))
        sys.exit(2)
    # Применяю события  для блокировки / разблокировки абонента SI2000 / 3000 через OpenMN
    logging.info(u'{}: Применяю события  для блокировки / разблокировки абонента SI 3000 через OpenMN'.format(p_atc))
    msg_email = '\nP_NUM: {}\nUSER_ID: {}\nID: {}\nClient IP: {}\n Event: {}  ERROR'.format(p_num, p_user, p_clsrv, client_ip, event)
    msg_email_ok = '\nP_NUM: {}\nUSER_ID: {}\nID: {}\nClient IP: {}\n Event: {}  OK'.format(p_num, p_user, p_clsrv, client_ip, event)
    omn = OpenMN()
    omn.ar_code = p_zone.replace(' ', '').split('-')[1]
    omn.dn_num = p_num


    if p_atsid is False or len(p_atsid) == 0:
        logging.info(u' получение ноды по номеру и коду города getNodeByNum() areaCode={} dn={}'.format(omn.ar_code, p_num))
        res = omn.getNodeByNum()
        logging.info(u'  getNodeByNum()-> результат p_atsid={}'.format(omn.p_atsid))
        if res == 'KeyError':
            mail.sendEmail(u'{}\nОшибка: Номер `{}` не существует или не является локальным номером станции ({})\n В ответе не найден ключ "subscriber" '.format(msg_email,p_num,omn.ar_code), 'ONYMA EVENT ERROR SI3000 {}'.format(p_zone), p_user_email)
            logging.info(u'{}\nНода по номеру и коду города не найдена areaCode={} dn={}\n В ответе не найден ключ "subscriber" '.format(msg_email,omn.ar_code, p_num))
            info = '{} ({})'.format(p_zone, omn.p_atsid)
            mysql_obj = mysql.MysqlDb()
            # setClientHistory(self, p_user=0, event='', p_clsrv='', py_answer='', client_id=0, p_ipnet='', p_num='', script_status='error', info='', p_zone='', p_atsid=''):
            mysql_obj.setClientHistory(p_user=p_user, event=event, p_clsrv=p_clsrv, p_num=p_num, script_status='error',
                                       info=info, p_zone=p_zone, p_atsid=omn.p_atsid, p_username=p_username)
            sys.exit(2)
    else:
        omn.p_atsid = p_atsid

    #print(omn.p_atsid)
    #print('-'*50)
    #sys.exit(2)

    # задаем условие цикла кол-во попыток
    attempts = 1

    status = 'error'
    while attempts < 2:
        attempts += 1
        if event == 'pause':
            res = omn.suspend()  # выкл.
            status = omn.status()
            if status != 'suspended':
                status = 'error'
                logging.info(u' omn.status() {} != suspended'.format(status))
                mail.sendEmail(msg_email, 'ONYMA EVENT ERROR SI3000 {}'.format(p_zone), p_user_email)
                continue
            else:
                logging.info(u' omn.status() {} == suspended'.format(status))
                mail.sendEmail(msg_email_ok, 'ONYMA EVENT OK SI3000 {}'.format(p_zone), p_user_email)
                break
        elif event == 'activate':

            # # test getWhiteBlacklist
            # status = omn.getWhiteBlacklist()
            # print ('test')
            # print ('test===============')
            # print (status)
            # print ('test===============')
            # sys.exit(2)
            # # test

            res = omn.resume()  # вкл.
            status = omn.status()
            if status != 'notSuspended':
                status = 'error'
                logging.info(u' omn.status() {} != notSuspended'.format(status))
                mail.sendEmail(msg_email, 'ONYMA EVENT ERROR SI3000 {}'.format(p_zone), p_user_email)
                continue
            else:
                logging.info(u' omn.status() {} == notSuspended'.format(status))
                mail.sendEmail(msg_email_ok, 'ONYMA EVENT OK SI3000 {}'.format(p_zone), p_user_email)
                break
        else:
            logging.error(u' event != pause event != activate')

    #suspend = omn.suspend()
    #print('status {}'.format(status))
    #print('-' * 50)
    #sys.exit(2)

    info = '{} ({})'.format(p_zone, omn.p_atsid)
    #logging.debug(status)
    mysql_obj = mysql.MysqlDb()
    # setClientHistory(self, p_user=0, event='', p_clsrv='', py_answer='', client_id=0, p_ipnet='', p_num='', script_status='error', info='', p_zone='', p_atsid=''):
    mysql_obj.setClientHistory(p_user=p_user, event=event, p_clsrv=p_clsrv, p_num=p_num, script_status=status, info=info, p_zone=p_zone, p_atsid=omn.p_atsid, p_username=p_username)
    sys.exit(2)


# // класс для блокировки / разблокировки абонента Si3000 через OpenMN
class OpenMN:

    def __init__(self):
        self.xml = ""
        self.p_atsid = ''  #Алматы-26303 ; Астана-1009; Атырау-27177; Уральск-25416; Актобе-25293; Актау-20141;
        self.ar_code = ''  # '727'
        self.dn_num = ''    # 2377451
        self.mn_ip = '#######'  #
        self.basic_user = '#######'
        self.basic_pass = '#######'
        self.p_tg = ''

#ОТКРЫТЫЕ МЕТОДЫ

    # получение ноды по номеру и коду города
    def getNodeByNum(self):
        self.xml = '''<urn:getSubscriberNodeInfoRequest>
            <subscriber areaCode="{}" dn="{}"/>
            </urn:getSubscriberNodeInfoRequest>'''.format(self.ar_code, self.dn_num)
        namespaces = {'SOAP-ENV:Envelope': None,
                      'SOAP-ENV:Body': None,
                      'http://schemas.xmlsoap.org/soap/envelope/': None,
                      'urn:iskratel-si:itnbsp-1-0': None,
                      'SOAP-ENV': None,
                      }

        answer = xmltodict.parse(self.doRequest(), process_namespaces=True, namespaces=namespaces)
        try:
            for i in answer['Envelope']['Body']['getSubscriberNodeInfoResponse']['subscriber']['param']:
                if i['@key'] == 'node':
                    self.p_atsid = i['@value']
                    return 'ok'
        except KeyError as e:
            logging.error(u' KeyError, получение ноды по номеру и коду города getNodeByNum() areaCode={} dn={} \n{}'.format(self.ar_code, self.dn_num, e))
            logging.error(u' KeyError, getNodeByNum()-> res=\n{}'.format(answer))

            return 'KeyError'

    def modifyWhiteBlacklist(self,params,is_remove=False,depth=None):
        if is_remove:
            delete_str = '<param key="operation" value="removeList"/>'
        else:
            delete_str = ''
        if depth == None:
            depth_str = '10'
        else:
            depth_str = str(depth)

        params_str=''
        if type(params) != list:
            temp_p = []
            temp_p.append(params)
            params = temp_p[:]
        if params:
            for param in params:
                params_str = params_str + '''<paramList>
                                            <param key="callingParty" value="{}"/>
                                            <param key="length" value="{}"/>
                                            <param key="lengthMode" value="exact"/>
                                            </paramList>'''.format(param,depth_str)
        self.xml = '''<urn:modifyWhiteBlackListOnTrunkGroupRequest>
            <node node="{}" trunkGroup="{}">
                <list listType="white">
                {}
                {}                  
                </list>
            </node>
      </urn:modifyWhiteBlackListOnTrunkGroupRequest>'''.format(self.p_atsid, self.p_tg,delete_str,params_str)

        namespaces = {'SOAP-ENV:Envelope': None,
                      'SOAP-ENV:Body': None,
                      'http://schemas.xmlsoap.org/soap/envelope/': None,
                      'urn:iskratel-si:itnbsp-1-0': None,
                      'SOAP-ENV': None,
                      }

        #print('SEND+++++++++++++++++++++++')
        #print(self.xml)
        #print('SEND+++++++++++++++++++++++')


        answer = xmltodict.parse(self.doRequest_2(), process_namespaces=True, namespaces=namespaces)

        #print('RECIEVE***********************')
        #print(answer)
        #print('RECIEVE***********************')

        try:
            status = answer['Envelope']['Body']['modifyWhiteBlackListOnTrunkGroupResponse']['error']
            #print('status {}'.format(status))
        except TypeError as e:
            pass
        else:
            return status
        try:
            status = answer['Envelope']['Body']['modifyWhiteBlackListOnTrunkGroupResponse']
            #print('status {}'.format(status))
        except TypeError as e:
            pass
        else:
            return status
        return 'error'

    # Получить белый и черный список
    def getWhiteBlacklist (self):
        self.xml = '''<urn:getWhiteBlackListOnTrunkGroupRequest>
                      <node node="{}" trunkGroup="{}"/>
                      </urn:getWhiteBlackListOnTrunkGroupRequest>'''.format(self.p_atsid, self.p_tg)

        namespaces = {'SOAP-ENV:Envelope': None,
                      'SOAP-ENV:Body': None,
                      'http://schemas.xmlsoap.org/soap/envelope/': None,
                      'urn:iskratel-si:itnbsp-1-0': None,
                      'SOAP-ENV': None,
                      }
        #print('SEND+++++++++++++++++++++++')
        #print(self.xml)
        #print('SEND+++++++++++++++++++++++')

        output = self.doRequest_2()
        #print('RECIEVE***********************')
        #print(output)
        #print('RECIEVE***********************')
        answer = xmltodict.parse(self.doRequest_2(), process_namespaces=True, namespaces=namespaces)
        #for i in answer['Envelope']['Body']['getSubscriberSuspensionStatusResponse']['subscriber']['param'][0]:
            #print(i)

        try:
            status = answer['Envelope']['Body']['getWhiteBlackListOnTrunkGroupResponse']
            #print('status {}'.format(status))
        except TypeError as e:
            pass
        else:
            return status

        try:
            status = answer['Envelope']['Body']['getWhiteBlackListOnTrunkGroupResponse']['error']
            #print('status {}'.format(status))
        except TypeError as e:
            pass
        else:
            return status


        return 'error'


    # проверка статуса блокировки
    def status(self):
        self.xml = '''<urn:getSubscriberSuspensionStatusRequest>
         <subscriber node="{}" areaCode="{}" dn="{}"/>
         </urn:getSubscriberSuspensionStatusRequest>'''.format(self.p_atsid, self.ar_code, self.dn_num)
        # print("========")
        # print(self.xml)
        # print("========")
        namespaces = {'SOAP-ENV:Envelope': None,
                      'SOAP-ENV:Body': None,
                      'http://schemas.xmlsoap.org/soap/envelope/': None,
                      'urn:iskratel-si:itnbsp-1-0': None,
                      'SOAP-ENV': None,
                      }
        answer = xmltodict.parse(self.doRequest(), process_namespaces=True, namespaces=namespaces)
        #for i in answer['Envelope']['Body']['getSubscriberSuspensionStatusResponse']['subscriber']['param'][0]:
            #print(i)
        # print('+++++++++++++++++++++++')
        # print(self.xml)
        # print('+++++++++++++++++++++++')

        output = self.doRequest()
        # print('***********************')
        # print(output)
        # print('***********************')
        try:
            status = answer['Envelope']['Body']['getSubscriberSuspensionStatusResponse']['subscriber']['param']['@value']
            #print('status {}'.format(status))
        except TypeError as e:
            pass
        else:
            return status
        try:
            status = answer['Envelope']['Body']['getSubscriberSuspensionStatusResponse']['subscriber']['param'][0]['@value']
            #print('status {}'.format(status))
        except TypeError as e:
            pass
        else:
            return status

        return 'error'
    # приостановка сервиса для абонента без блокировки экстренных вызовов
    def suspend(self):
        supplSrvSetId = self.getSuspensoinType()
        self.xml = '''<urn:modifySubscriberSuspensionRequest>
        <subscriber node="{}" areaCode="{}" dn="{}" status="suspend">
        <param key="supplSrvSetId" value="{}"/>
        <param key="supplSrvSetName" value="Suspension Soft"/>
        <param key="type" value="out_inc"/>
        </subscriber>
        </urn:modifySubscriberSuspensionRequest> '''.format(self.p_atsid, self.ar_code, self.dn_num, supplSrvSetId)
        return self.doRequest()

    # восстановление сервиса
    def resume(self):
        self.xml = '''<urn:modifySubscriberSuspension>
        <subscriber node = "{}" areaCode = "{}" dn = "{}" status = "resume"/>
        </urn:modifySubscriberSuspension> '''.format(self.p_atsid, self.ar_code, self.dn_num)
        return self.doRequest()

# ВНУТРЕННИЕ МЕТОДЫ - -------------------------

    # запрос типа приостановки для абонента, чтобы использовать в modifySubscriberSuspension
    def getSuspensoinType(self):
        self.xml = '''<urn:getNodeSupplementaryServiceSetRequest>
        <subscriber node="{}" areaCode="{}" dn="{}" usage="suspension"/>
        </urn:getNodeSupplementaryServiceSetRequest> '''.format(self.p_atsid, self.ar_code, self.dn_num)
        namespaces = {'SOAP-ENV:Envelope': None,
                      'SOAP-ENV:Body': None,
                      'http://schemas.xmlsoap.org/soap/envelope/': None,
                      'urn:iskratel-si:itnbsp-1-0': None,
                      'SOAP-ENV': None,
                      }
        answer = xmltodict.parse(self.doRequest(), process_namespaces=True, namespaces=namespaces)
        try:
            sus_type = answer['Envelope']['Body']['getNodeSupplementaryServiceSetResponse']['subscriber']['supplSrvSetList']['param'][1]['@supplSrvSetId']
        except KeyError as e:
            sys.exit(2)
        else:
            return sus_type

    #  отправка запроса к MN private
    def doRequest(self):
        soap = '''<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:iskratel-si:itnbsp-1-0">
        <soapenv:Header/>
        <soapenv:Body>
         {}
        </soapenv:Body>
        </soapenv:Envelope>'''.format(self.xml)
        #print(soap)
        #print('-'*50)
        try:
            #auth_data = 'Basic {}'.format(base64.b64encode('{}:{}'.format(self.basic_user, self.basic_pass)))
            #headers = {'content-type': 'application/json','user-agent': 'Opera/9.63 (Windows NT 5.1; U; ru) Presto/2.1.1','authorization': auth_data}
            # headers = ''
            url = 'https://{}/openmn/nb/NBProvisioningWebService'.format(self.mn_ip)
            r = requests.post(url, data=soap, auth=requests.auth.HTTPBasicAuth(self.basic_user, self.basic_pass), timeout=10.0, verify=False)
            #print(r.headers)
            #print(r.content)
            #r = requests.post(url, headers=headers, timeout=5.0, verify=False)

            #r = requests.post(url, headers=headers, data=soap, auth=(self.basic_user, self.basic_pass), timeout=5.0, verify=False)
        except requests.exceptions.RequestException as e:
            logging.error('{}\nhttps://{}/openmn/nb/NBProvisioningWebService'.format(e, self.mn_ip))
            return 'error timeout'
        else:

            return r.text

    #  отправка запроса к http://192.168.34.2/openmn/nb/NBProvisioningWebService
    def doRequest_2(self):
        soap = '''<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:iskratel-si:itnbsp-1-0">
        <soapenv:Header/>
        <soapenv:Body>
         {}
        </soapenv:Body>
        </soapenv:Envelope>'''.format(self.xml)
        #print(soap)
        #print('-'*50)
        try:
            #auth_data = 'Basic {}'.format(base64.b64encode('{}:{}'.format(self.basic_user, self.basic_pass)))
            #headers = {'content-type': 'application/json','user-agent': 'Opera/9.63 (Windows NT 5.1; U; ru) Presto/2.1.1','authorization': auth_data}
            # headers = ''
            url = 'http://192.168.34.2/openmn/nb/NBProvisioningWebService'.format(self.mn_ip)
            r = requests.post(url, data=soap, auth=requests.auth.HTTPBasicAuth(self.basic_user, self.basic_pass), timeout=10.0, verify=False)
            #print(r.headers)
            #print(r.content)
            #r = requests.post(url, headers=headers, timeout=5.0, verify=False)

            #r = requests.post(url, headers=headers, data=soap, auth=(self.basic_user, self.basic_pass), timeout=5.0, verify=False)
        except requests.exceptions.RequestException as e:
            logging.error('{}\nhttps://{}/openmn/nb/NBProvisioningWebService'.format(e, self.mn_ip))
            return 'error timeout'
        else:

            return r.text